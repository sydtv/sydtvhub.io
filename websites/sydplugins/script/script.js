// Parallax
function parallax() {
    var scrollpos = $(this).scrollTop();
    var headheight = $('.header').outerHeight();
    $('.restof').css('top', ('-'+scrollpos*0.7)+'px');
    $('.restof').css('margin-top', headheight);
}


// Sticky Nav
function stickyNav() {
    var targetScrollpos = $('.content').offset().top;
    var scrollpos = $(this).scrollTop();
    if (scrollpos > targetScrollpos) {
        $('.con-nav').addClass('fixed-nav');
        $('.con1').css('padding-top', 150);
    } else {
        $('.con-nav').removeClass('fixed-nav');
        $('.con1').css('padding-top', 50);
    }
}


(function ($) {

    $(window).scroll(function () {
        stickyNav();
        parallax();
    });

    $(window).resize(function () {
        parallax();
    });

    $('.active').click(function (e) {
        var linkHref = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(linkHref).offset().top}, 1000);
        e.preventDefault();
    });

    parallax();
    stickyNav();

}(jQuery));
